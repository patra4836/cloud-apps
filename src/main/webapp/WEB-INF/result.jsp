<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="author" content="Ashish">
<meta name="copyright" content="company name">
<meta name="robots" content="index,follow">
<title>Cloud Application</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="144x144" type="image/x-icon"
	href="images/favicon/apple-touch-icon.png">

<!-- All CSS Plugins -->
<link rel="stylesheet" type="text/css" href="css/plugin.css">

<!-- Main CSS Stylesheet -->
<link rel="stylesheet" type="text/css" href="css/style.css">

<!-- Google Web Fonts  -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700">


<!-- HTML5 shiv and Respond.js support IE8 or Older for HTML5 elements and media queries -->
<!--[if lt IE 9]>
	   <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<!-- Menu Section Start -->
	<header id="home">
		<div class="header-top-area navigation-background">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 margin-top-10">
						<div class="text-center">
							<h1 style="color: #4c9cef">Cloud App</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Home Section Start -->
	<section class="home-section">
		<div class="container">
			<div class="row">

				<div class="col-sm-6 margin-left-setting">
					<div class="margin-top-50">
						<div class="table-responsive">
							<table class="table">
								<tr>
									<td>Email</td>
									<td>${user.emailId}</td>
								</tr>
								<tr>
									<td>Name</td>
									<td>${user.firstName} ${user.lastName}</td>
								</tr>
								<tr>
									<td>Designation</td>
									<td>${user.designation}</td>
								</tr>
								<tr>
									<td>Contact</td>
									<td>${user.contactNum}</td>
								</tr>
								<tr>
									<td>Resume</td>
									<td style="background-color: #f7639a;"><a href="#"
										target="_blank" data-toggle="tooltip" data-placement="top"
										title="Check Out My Resume">Resume.pdf</a></td>
								</tr>
							</table>
						</div>
						<div class="text-center margin-top-50">
							<a
								class="button button-style button-style-dark button-style-color-2 smoth-scroll"
								href="./index.html">Go Back!</a>
						</div>
					</div>
				</div>

				<div class="col-md-5 col-sm-6">
					<div class="margin-top-50">
						<div class="me-image">
							<img src="images/bg/profile.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Home Section End -->
	<footer class="footer-section">
		<div class="container">
			<div class="row">

				<div class="col-md-12"></div>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="js/jquery.min.js"></script>

</body>
</html>